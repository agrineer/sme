#  crop_reader.py
# 
#  Copyright (C) 2018 IndieCompLabs, LLC.
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html
#

crop_reader_copyright = 'crop_reader.py Copyright (c) 2018 IndieCompLabs, LLC., released under GNU GPL V3.0'

import os

## @file      crop_reader.py
## @brief     Class to ingest and deliver crop attributes. 
##            Specifically, seasonally dependent ET coefficients, 
##            and GDD temperature parameters.
## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

# TODO: read wilt point and test against reported soil moisture

## sme helper class to read in crops, season length, coefficients, etc.
class crop_reader():

    ## Constructor
    ## @param logger - instantiated logger class to use (gui, non-gui)
    def __init__( self, logger ):

        ## use this logger (could be gui or not)
        self.logger = logger
        self.clear()

    ## Sets seasonal crop coefficients and associated time lengths
    ## and reports to logger
    ## @param info - information string from input file
    ## @return boolean success state 
    def load_Kci( self, info ):

        # load values into readable vars

        # seasonal Kc's
        Kcini = info[1]
        Kcmid = info[2]
        Kcend = info[3]

        # stage lengths
        ilen = info[4]
        dlen = info[5]
        mlen = info[6]
        llen = info[7]

        tlen = ilen + dlen + mlen + llen

        # calculate Kci according to seasonal stage
        self.Kci = []
        for day in range(1,int(tlen)+1):    
    
            if day < ilen:
                Kci = Kcini

            elif day < (ilen+dlen):
                Sps = ilen
                Sl = dlen
                Kcp = Kcini
                Kcn = Kcmid

                Kci = Kcp + ((day-Sps)/Sl)*(Kcn-Kcp)

            elif day < (ilen+dlen+mlen):
                Kci = Kcmid

            elif day < tlen+1:
                Sps = ilen + dlen + mlen
                Sl = llen
                Kcp = Kcmid
                Kcn = Kcend

                Kci = Kcp + ((day-Sps)/Sl)*(Kcn-Kcp)

            self.Kci.append( Kci )

        # report
        self.logger.append( '\n Crop Attributes:\n' )
        self.logger.append( '\tKcini:\t\t\t\t' + str(Kcini) + '\n')
        self.logger.append( '\tKcmid:\t\t\t\t' + str(Kcmid) + '\n')
        self.logger.append( '\tKcend:\t\t\t\t' + str(Kcend) + '\n\n')

        self.logger.append( '\tLength ini:\t\t\t' + str(int(ilen)) + ' days\n')
        self.logger.append( '\tLength dev:\t\t\t' + str(int(dlen)) + ' days\n')
        self.logger.append( '\tLength mid:\t\t\t' + str(int(mlen)) + ' days\n')
        self.logger.append( '\tLength late:\t\t' + str(int(llen)) + ' days\n')
        self.logger.append( '\tLength total:\t\t' + str(int(tlen)) + ' days\n')

        return True

    ## Load grow degree day parameter temps from input line
    ## @param info - information string from input file
    ## @returns boolean success state 
    def load_temp_params( self, info ):
        basetemp = float(info[8])
        uppertemp = float(info[9])
        if uppertemp <= basetemp:
            print('ERROR: uppertemp cannot be <= basetemp\n',
                  file=sys.stderr, flush=True)
            print('basetemp =', basetemp, 'uppertemp =', uppertemp,
                  file=sys.stderr, flush=True)
            self.logger.append( '\tERROR: uppertemp cannot be <= basetemp\n' )
            self.logger.append( 'basetemp =' + '%.1f'%basetemp +  
                                ' uppertemp =' + '%.1f'%uppertemp )
            return False

        self.basetemp = basetemp
        self.uppertemp = uppertemp

        self.logger.append('\n\tCrop GDD basetemp:\t\t' + '%.1f'%basetemp + 
                           'C\n') 
        self.logger.append('\tCrop GDD uppertemp:\t\t' + '%.1f'%uppertemp +
                           'C\n') 
        return True

    ## Read crop types from input file
    ## @param filepath - path to crop list (with attributes) file
    ## @returns boolean success state
    # TODO: implement crop wilting point values in input
    def read( self, filepath ):

        if not os.path.isfile( filepath ) :
            self.logger.append( 'read: given file: ' + 
                                filepath + ' does not exist\n' )
            return False

        # check read permissions
        if not os.access( filepath, os.R_OK ):
            self.logger( 'cannot read given file:', filepath + '\n' )
            return False

        f = open( filepath, 'r' )

        # skip header, look for 'Crop'
        for line in f:
            entries = line.split(',')
            if (entries[0] == 'Crop') or (entries[0] == 'crop') :
                break

        # read values into info list
        self.info = [] # start fresh
        for line in f:
            entries = line.split(',')
            
            # keep first entry as string (cropname)
            for i in range(1,9): # read up to 9th entry 1-8 
                entries[i] = float( entries[i]   ) # make floats
                
            # load this crop into info array
            self.info.append( entries )

        f.close()
        return True
    
    ## Clear out variable to None
    def clear( self ):

        ## Crop information array
        self.info = None        

        ## Daily seasonal crop coefficients array
        self.Kci = None         

        ## Selected crop name
        self.name = None        

        ## Date crop was planted
        self.planted = None     

        ## GDD base temp for selected crop
        self.basetemp = None    

        ## GDD upper temp for this crop
        self.uppertemp = None   

# end class crop_reader

# end crop_reader.py
