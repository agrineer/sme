## Description
This package contains command-line driven Python and C code which estimates soil moisture values, currently using:  

  - Weather, Research, and  Forecast (WRF) model data,
  - crop indexed evapotranspiration (ETc) values calculated using the UN's Food and Agriculture Organization (FAO) guidelines, and 
  - Sacramento Soil Moisture Accounting method (SMA).

Users can specify the location, run dates, crop, and soil types, in addition to local rain and irrigation events, for an estimate of soil water content at different soil levels (currently just two, corresponding to the "A" (topsoil) and "B" (subsoil) horizons.

A GUI implementation of this package is on the Agrineer.org [website](https://agrineer.org/sme/sme.php) and covers about half of the western United States, at ~3km pixel resolution.

The package was developed using GNU/Linux Mint 17, but other 
Debian platforms are known to work (Ubuntu 12,14; Mint 18,19).

The Soil Moisture Estimator (SME) package contains two components.

One component is the "sme.py" program which coordinates the required data for the other component, a specified soil moisture model.

We initiate this package with the well-studied 
"Sacramento Soil Moisture Accounting" (SMA) method as the back-end soil moisture model. Other soil moisture models will be available in the future.

Users are encouraged to vary model components, code, 
and parameters in order to further the understanding and accuracies of hydrological soil modeling, in particular for agricultural purposes. 

### Current Implementation Stream
Briefly:  

Modelled Weather -> Large Scale ETo -> Local ETc/Soil/Rain/Irr -> Calc Soil Moisture

This initial release uses reanalyzed Global Forecasting System (GFS) data as input to the WRF model to produce modelled, hindcast weather data; although other models and input data can be used, including forecast data.

The output data from the WRF model is used to calculate standard  evapotranspiration [ETo](https://gitlab.com/agrineer/eto) according to FAO guidelines, over all of a sector's pixels on an hourly basis, 
for a given period of days. Then, the sme.py program calculates the ETc value (also per FAO) for a specified location, on a daily basis accumulated hourly for a crop of interest and is reported together with modelled rain events. 

The reported atmospheric load data (precip and ETc) is now used in soil moisture model to calculate water content of the soil over the given period of days. Irrigation events can be included at this point and modelled rain events can be overridden with local data.

The Sacramento Soil Moisture Accounting (SMA) model is currently implemented in this release and delivers moisture content estimates in the soil at two levels (topsoil and subsoil).

### Flexible Approach
The implemented stream allows a programmer to replace the GFS data, say, with the North American Model (NAM) data, in the WRF model or even replace WRF altogether with another weather model if desired, as long as data input/output formats are satisfied (with some programming work, of course). 

Additionally, all components are open-sourced, so a programmer can tweak or replace the weather, ETo, and ETc calculations for improvements in code and accuracies at a more granular level. This becomes important when considering local conditions and/or cultivars and is the focus of this project.

Finally, other soil moisture models can be used in place 
of the SMA, provided that an interface is given that reads the sme.py atmosphereic load output data.

### Goal
Our goal is to create a community based, hydrological atmosphere-soil model platform that is large scale, yet customizable for local environments and crops, with the aim to improve agricultural water management.  

To achieve this goal, our current implementation is necessary but not sufficient. The values calculated by the SME package set a reasonable place to start improvements, but in order to do so verification (actual soil moisture) values are required for a convergence process. 

Our next phase, soon to come, is to conduct field studies using formal scientific methods and then to extend to citizen-scientists lessons learned for a much wider sampling size.

## Installation
### Dependencies 
   - The [agrineer](https://gitlab.com/agrineer/agrineer) package must be installed.  
   
   - WRF/ETo data files, retrieved by programs in the "agrineer" package. 

### Setup
   Determine a directory to house the SME project, eg. /home/user/projects.

   Change into this directory and either unpack or clone this project.  
   
         > cd /home/user/projects  
         
         > tar xvf sme.tar
         or 
         > git clone https://gitlab.com/agrineer/sme

   You will need to compile the C programs in the "sacramento" directory, eg.  
   
        > cd /home/user/projects/sme/sacramento
        > make
        > cd ..             # back to sme dir

   This will produce a binary executable "sma" and is placed in the parent "sme" directory.

### Running the sme.py program 

   The sme.py program requires WRF/ETo data and a configuration file. Data can be downloaded using programs in the "agrineer" package.

   You will also need to create a "results" directory to direct sme output into, eg.  
   
       > mkdir somewhere/results
and use this in the configuration file. An example configuration file is given in the "params" directory 
   called "agrineer.conf". Copy this file, eg.  
   
       > cd params
       > cp agrineer.conf MySme.conf
       
 
and modify the input values for your environment. Be sure to point to the newly made "results" directory to direct sme output into.

   Details on input values can be found at our [wiki](https://www.agrineer.org/wiki/index.php?title=Soil_Moisture_Estimator). Make sure sme.py has execution priviledges.

   Run:  
   
        > cd ..    # sme dir
        > ./sme.py -c params/MySme.conf

Look in the output directory specified in the configuration file for the result output.
