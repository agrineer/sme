#! /usr/bin/env /usr/bin/python3

#  validate.py
# 
#  Copyright (c) IndieCompLabs, LLC.
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html

validate_copyright = 'validate.py Copyright (c) 2018 IndieComplabs, LLC.,  released under GNU GPL V3.0'

import os
import sys
import getopt

## @file      validate.py
## @brief     Program to validate web SME form entries
## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

from agrineer import wrfbase

def usage():
    print('usage: validate.py', file=sys.stderr,flush=True)
    print('       -h, --help', file=sys.stderr,flush=True)
    print('       -a latititude, --lat=latitude',
          file=sys.stderr,flush=True)
    print('       -o longitude, --lon=longitude',
          file=sys.stderr,flush=True)
    print('       -p plantdate, --plant=plantdate',
          file=sys.stderr,flush=True)
    print('       -s startdate, --start=startdate',
          file=sys.stderr,flush=True)
    print('       -e enddate, --end=enddate',
          file=sys.stderr,flush=True)
    
# end usage
            
def set_params( argv ):
    lat = None
    lon = None
    plant = None
    start = None
    end = None

    try:                                
        opts, args = getopt.getopt( argv, 'ha:o:p:s:e:',
                                    ['help','lat=','lon=',
                                     'plant=','start=','end=' ] )
    except:
        usage()                          
        sys.exit(2)
                   
    for opt, arg in opts:                
        if opt in ( '-h', '--help' ):      
            usage()                     
            sys.exit(2) 

        elif opt in ( '-a', '--lat' ):
            if ( float( arg ) >= -90.0 and float( arg ) <= 90.0 ) :
                lat = float(arg)

        elif opt in ( '-o', '--lon' ):
            if ( float( arg ) >= -180.0 and float( arg ) <= 180.0 ):
                lon = float(arg)

        elif opt in ( '-p', '--plant' ):
            if len( arg ) == 8:
                plant = arg

        elif opt in ( '-s', '--start' ):
            if len( arg ) == 8:
                start = arg

        elif opt in ( '-e', '--end' ):
            if len( arg ) == 8:
                end = arg

    if lat == None:
        print('validate.py: must specify valid latitude',
              file=sys.stderr,flush=True)
        sys.exit(12)     

    if lat == None:
        print('validate.py: must specify valid longitude',
              file=sys.stderr,flush=True)
        sys.exit(12)       

    if plant == None:
        print('validate.py: must specify valid plant date',
              file=sys.stderr,flush=True)
        sys.exit(13)       

    if start == None:
        print('validate.py: must specify valid start date',
              file=sys.stderr,flush=True)
        sys.exit(13)       

    if end == None:
        print('validate.py: must specify valid end date',
              file=sys.stderr,flush=True)
        sys.exit(13)       

    return lat,lon,plant,start,end

# end set_params

if __name__ == '__main__':  

    # command line gives parameters
    lat,lon,plant,start,end = set_params( sys.argv[1:] )  
    
    w = wrfbase()             # instantiate wrfbase class
    w.load_dirs( '/home/agrineer/SMVdata', '/tmp' )

    sector = w.find_sector( lat, lon )
    if sector == None:
        print('no sector found')
        sys.exit(12)          # exit 12 indicates bad sector

    try:
        w.get_flist_dlist( plant, start, 'd03', sector )
    except:
        print('sector:', sector, file=sys.stderr, flush=True)
        print('bad rundates:', plant, start, file=sys.stderr, flush=True)
        sys.exit(13)          # exit 13 indicates bad rundates

    try:
        w.get_flist_dlist( start, end, 'd03', sector )
    except:
        print('sector:', sector,file=sys.stderr, flush=True)
        print('bad rundates:', start, end,file=sys.stderr, flush=True)
        sys.exit(13)          # exit 13 indicates bad rundates

    print('sector:', sector,file=sys.stderr, flush=True)
    print('rundates:', plant, start, end,file=sys.stderr, flush=True)
    
    sys.exit(0)

# end __main__

# end validate.py
