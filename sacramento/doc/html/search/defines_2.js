var searchData=
[
  ['ini_5fallow_5fbom',['INI_ALLOW_BOM',['../ini_8h.html#ac7397d484a14bb9007527edd0b66e38c',1,'ini.h']]],
  ['ini_5fallow_5finline_5fcomments',['INI_ALLOW_INLINE_COMMENTS',['../ini_8h.html#a9443b06de7678499c7b68148359e8fe0',1,'ini.h']]],
  ['ini_5fallow_5fmultiline',['INI_ALLOW_MULTILINE',['../ini_8h.html#ad0f757e985620449b325e2023ee21275',1,'ini.h']]],
  ['ini_5fallow_5frealloc',['INI_ALLOW_REALLOC',['../ini_8h.html#ae12f79b788d1181024f0aabe6f444b36',1,'ini.h']]],
  ['ini_5fhandler_5flineno',['INI_HANDLER_LINENO',['../ini_8h.html#a1b9ae2ea08de2b455f0db240a9385afe',1,'ini.h']]],
  ['ini_5finitial_5falloc',['INI_INITIAL_ALLOC',['../ini_8h.html#ad8a81c44a7c94dc99dab9004e93670da',1,'ini.h']]],
  ['ini_5finline_5fcomment_5fprefixes',['INI_INLINE_COMMENT_PREFIXES',['../ini_8h.html#a327d5a588a6a54129f768fc8dcc6f115',1,'ini.h']]],
  ['ini_5fmax_5fline',['INI_MAX_LINE',['../ini_8h.html#a5c067971ef2b8e9d35b716dd773eb1bf',1,'ini.h']]],
  ['ini_5fstart_5fcomment_5fprefixes',['INI_START_COMMENT_PREFIXES',['../ini_8h.html#a94fd267a11de14467754cd87e51a9f28',1,'ini.h']]],
  ['ini_5fstop_5fon_5ffirst_5ferror',['INI_STOP_ON_FIRST_ERROR',['../ini_8h.html#a197006c50f50af690746459a73de14a3',1,'ini.h']]],
  ['ini_5fuse_5fstack',['INI_USE_STACK',['../ini_8h.html#a2a6d4557273d55fd770d40c690094aa1',1,'ini.h']]]
];
