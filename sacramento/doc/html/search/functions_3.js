var searchData=
[
  ['ini_5fparse',['ini_parse',['../ini_8c.html#a2fab292681265b7774e2a2885abb758c',1,'ini_parse(const char *filename, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#a2fab292681265b7774e2a2885abb758c',1,'ini_parse(const char *filename, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5ffile',['ini_parse_file',['../ini_8c.html#a876bf8b62ade275320db827b929f9289',1,'ini_parse_file(FILE *file, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#a876bf8b62ade275320db827b929f9289',1,'ini_parse_file(FILE *file, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5fstream',['ini_parse_stream',['../ini_8c.html#ad8b766ac18397b0b262559203ea7f602',1,'ini_parse_stream(ini_reader reader, void *stream, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#ad8b766ac18397b0b262559203ea7f602',1,'ini_parse_stream(ini_reader reader, void *stream, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5fstring',['ini_parse_string',['../ini_8c.html#aeb90a0b5e4226351899d28ead4eed6fa',1,'ini_parse_string(const char *string, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#aeb90a0b5e4226351899d28ead4eed6fa',1,'ini_parse_string(const char *string, ini_handler handler, void *user):&#160;ini.c']]]
];
