var searchData=
[
  ['ini_2ec',['ini.c',['../ini_8c.html',1,'']]],
  ['ini_2eh',['ini.h',['../ini_8h.html',1,'']]],
  ['ini_5fallow_5fbom',['INI_ALLOW_BOM',['../ini_8h.html#ac7397d484a14bb9007527edd0b66e38c',1,'ini.h']]],
  ['ini_5fallow_5finline_5fcomments',['INI_ALLOW_INLINE_COMMENTS',['../ini_8h.html#a9443b06de7678499c7b68148359e8fe0',1,'ini.h']]],
  ['ini_5fallow_5fmultiline',['INI_ALLOW_MULTILINE',['../ini_8h.html#ad0f757e985620449b325e2023ee21275',1,'ini.h']]],
  ['ini_5fallow_5frealloc',['INI_ALLOW_REALLOC',['../ini_8h.html#ae12f79b788d1181024f0aabe6f444b36',1,'ini.h']]],
  ['ini_5fhandler',['ini_handler',['../ini_8h.html#a0dab144ac063ea70dafe649ba9e5074b',1,'ini.h']]],
  ['ini_5fhandler_5flineno',['INI_HANDLER_LINENO',['../ini_8h.html#a1b9ae2ea08de2b455f0db240a9385afe',1,'ini.h']]],
  ['ini_5finitial_5falloc',['INI_INITIAL_ALLOC',['../ini_8h.html#ad8a81c44a7c94dc99dab9004e93670da',1,'ini.h']]],
  ['ini_5finline_5fcomment_5fprefixes',['INI_INLINE_COMMENT_PREFIXES',['../ini_8h.html#a327d5a588a6a54129f768fc8dcc6f115',1,'ini.h']]],
  ['ini_5fmax_5fline',['INI_MAX_LINE',['../ini_8h.html#a5c067971ef2b8e9d35b716dd773eb1bf',1,'ini.h']]],
  ['ini_5fparse',['ini_parse',['../ini_8c.html#a2fab292681265b7774e2a2885abb758c',1,'ini_parse(const char *filename, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#a2fab292681265b7774e2a2885abb758c',1,'ini_parse(const char *filename, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5ffile',['ini_parse_file',['../ini_8c.html#a876bf8b62ade275320db827b929f9289',1,'ini_parse_file(FILE *file, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#a876bf8b62ade275320db827b929f9289',1,'ini_parse_file(FILE *file, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5fstream',['ini_parse_stream',['../ini_8c.html#ad8b766ac18397b0b262559203ea7f602',1,'ini_parse_stream(ini_reader reader, void *stream, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#ad8b766ac18397b0b262559203ea7f602',1,'ini_parse_stream(ini_reader reader, void *stream, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5fstring',['ini_parse_string',['../ini_8c.html#aeb90a0b5e4226351899d28ead4eed6fa',1,'ini_parse_string(const char *string, ini_handler handler, void *user):&#160;ini.c'],['../ini_8h.html#aeb90a0b5e4226351899d28ead4eed6fa',1,'ini_parse_string(const char *string, ini_handler handler, void *user):&#160;ini.c']]],
  ['ini_5fparse_5fstring_5fctx',['ini_parse_string_ctx',['../structini__parse__string__ctx.html',1,'']]],
  ['ini_5freader',['ini_reader',['../ini_8h.html#a0d2fc89e4214e50034cd9cef9287a80c',1,'ini.h']]],
  ['ini_5fstart_5fcomment_5fprefixes',['INI_START_COMMENT_PREFIXES',['../ini_8h.html#a94fd267a11de14467754cd87e51a9f28',1,'ini.h']]],
  ['ini_5fstop_5fon_5ffirst_5ferror',['INI_STOP_ON_FIRST_ERROR',['../ini_8h.html#a197006c50f50af690746459a73de14a3',1,'ini.h']]],
  ['ini_5fuse_5fstack',['INI_USE_STACK',['../ini_8h.html#a2a6d4557273d55fd770d40c690094aa1',1,'ini.h']]],
  ['irr_5ffile',['irr_file',['../structconfiguration.html#a9cabbb567099ee9bb3287de5ff6b2b3a',1,'configuration']]]
];
