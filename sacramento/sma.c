/* sma.c

  Copyright (C) 2018-2019 IndieCompLabs, LLC.
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html
*/

/* FIXME: doxygen tables not cooperating */

/**
  @file      sma.c 
  @author    Scott L. Williams
  @copyright Copyright (c) 2018-2019 IndieCompLabs, LLC. All Rights Reserved.
  @license   Released under GNU General Public License V3.0.
 
  @brief This program is a front-end C interface to the National Weather 
         Service/University of Arizona Sacramento Soil Moisture Accounting 
	 (SMA) model. This program prepares the input for the SMA
         modules and delivers the results in a file.
 
  The SMA module files: 
  - sacramento_state.c (a version of sac_sma.c)
  - fland1.c 
 
  were originally part of the MOSCEM package developed at 
  University of Arizona by Yuqiong Liu and others. Permission to use 
  and distribute these files was granted by Professor Hoshin Gupta 
  (University of Arizona) on 2009-08-28.
 
 
  The Sacramento Soil Moisture Accounting (SMA) model was originally developed
  by the US National Weather Service. The above files were downloaded from 
  Google's cached copy of
  http://info.science.uva.nl/ibed/research/Research_Fields/cbpg/software/code/moscem.0.tar.gz
  2009-08-20 by Felix Andrews.
 
  IndieCompLabs, LLC. makes no claim on sac_sma.c (sacramento_state.c)
  or fland1.c
  
  For an R language version go to http://hydromad.catchment.org. Many 
  descriptive phrases come from Hydromad.
 
  Refer to the file "paper_source_links.txt" in this package for papers on how
  the model works.
  
  To create the sma executable, type "make" in the same directory that
  contains the sma.c code. The executable is moved to its parent directory for
  use.
 
  Usage: 
  usage: sma -c config_file \n\n
  
  Where the configuration file holds "ini" parameters for: \n\n
  loads - Filepath to atmospheric loads (precip and et) data, described below.\n
  soil - Filepath to soil attribute data, described below.\n
  output - Filepath to output report.
  rain - Optional filepath for local rain input data. Overrides climate model rain data.\n
  irr - Optional filepath for irrigation events. Events count as precipitation and are added to the precip input data. \n\n
  
  Input file documention can be found in the routines:\n\n
  - read_loads() \n
  - read_soils() \n
  - replace_precip() \n
  - add_irrigations() \n\n
 
  Output file documentation can be found in the main() routine.\n

  Go to the wiki page https://www.agrineer.org/wiki/index.php?title=Sacramento_SMA for guidelines and other details.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ini.h"
#include "sacramento_state.h"

static char *sma_copyright = "sma.c Copyright (c) 2018 IndieCompLabs, LLC., \
released under GNU GPL V3.0.";

static char date[2048][64];        // date, precip and evaporation values
static double P[2048], E[2048];    // FIXME: implement memory allocation 

// configuration file specified
static char load_file[2048];       // mandatory atmospheric load file 
                                   // containing time-series precip, evap data.
static char soil_file[2048];       // mandatory soil attribute file
static char out_file[2048];        // mandatory output file

static char rain_file[2048];       // optional local rain input file
static char irr_file[2048];        // optional irrigation file

typedef struct {
  const char* load_file;           // atmospheric load
  const char* soil_file;           // soil type
  const char* out_file;            // output file
  const char* rain_file;           // local rain events
  const char* irr_file;            // irrigation events
} configuration;

/** 
 * @brief Print to screen this program's argument list.
 */
void usage( void ) 
{
  fprintf( stderr, "usage: sma -c config_file\n" );
  exit( EXIT_FAILURE );
} 

static int handler(void* config, const char* section, const char* name,
                   const char* value)
{
    configuration* pconfig = (configuration*)config;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

    if (MATCH("FILES", "load")) {
        pconfig->load_file = strdup(value);
    } else if (MATCH("FILES", "soil")) {
        pconfig->soil_file = strdup(value);
    } else if (MATCH("FILES", "out")) {
        pconfig->out_file = strdup(value);
    } else if (MATCH("FILES", "rain")) {
        pconfig->rain_file = strdup(value);
    } else if (MATCH("FILES", "irr")) {
        pconfig->irr_file = strdup(value);
    } else {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

/**
   @brief Parses the configuration file arguments and 
          sets the filenames internally as static global strings.
   @param argc - number of strings
   @param argv - string array
 */
void set_files( int argc, char *argv[] )
{
  if ( argc != 3 ) {
      fprintf( stderr, "set_files: wrong number of arguments\n" );
      usage();
  }

  int p;    
  char conf_file[2048];

  // pick off configuration file
  while ( (p = getopt( argc, argv, "c:")) != -1 ) {
    switch( p ) {
    case 'c':
      strcpy( conf_file, optarg );
      break;
    default:
      fprintf( stderr, "setfiles: unknown option: %d\n", p );
      usage();
    }
  }

  // set global file variables to NULL
  strcpy( load_file, "" );
  strcpy( soil_file, "" );
  strcpy( out_file, "" );
  strcpy( rain_file, "" );
  strcpy( irr_file, "" );

  // get filenames from configuration file
  configuration config;

  if (ini_parse( conf_file, handler, &config) < 0) {
    fprintf( stderr,"Can't load %s\n", conf_file );
    exit( EXIT_FAILURE );
  }

  // atmospheric load file
  if ( strcmp(config.load_file,"") == 0 ) {
    fprintf( stderr, "set_files: must have atmospheric load filename.\n" );
    exit( EXIT_FAILURE );
  }

  strcpy( load_file, config.load_file );

  // soil type file
  if ( strcmp(config.soil_file,"") == 0 ) {
    fprintf( stderr, "set_files: must have soils attribute filename.\n" );
    exit( EXIT_FAILURE );
  }

  strcpy( soil_file, config.soil_file );

  // output file
  if ( strcmp(config.out_file,"") == 0 ) {
    fprintf( stderr, "set_files: must have output filename.\n" );
    exit( EXIT_FAILURE );
  }

  strcpy( out_file, config.out_file );

  // rainfile and irrigation files are optional; just copy
  strcpy( rain_file, config.rain_file );
  strcpy( irr_file, config.irr_file );
}

/**
   @brief Utility function to get field value from CSV string.
   @param line - line string from file containing CSV string.
   @param num - field index into string.
 */
const char *getfield( char *line, int num )
{
    char *tmp = strdup(line);

    const char* tok;
    for (tok = strtok(tmp, ",");
      tok && *tok;
      tok = strtok(NULL, ",\n")) {
           if (!--num)
             return tok;
    }

    return NULL;
}

/**
  @brief Reads the input loads file consisting of precipitation and 
         evapotranspiration (et) amounts on a daily basis.

  The input file consists of a title line followed by value lines. Line values
  are comma separated (CSV) and the values are for date, precipitation, and 
  et. Precipitation and et are given in millimeters. For example, \n \n
  Date,P,E \n
  2018-01-02,0.00,1.0348 \n
  2018-01-03,0.00,0.8603 \n
  2018-01-04,0.00,0.8630 \n
  . \n
  . \n
  . \n
  @param fname - filename for the input load parameters
 */
int read_loads( char *fname )
{
  char line[2048];

  // open file containing date, precip and evap values
  // we ignore actual dates, entries are day based
  FILE *f = fopen( fname, "r" );
  if ( f == (FILE *)NULL )  {
    fprintf( stderr, "read_loads: error while opening the file %s.\n", fname );
    exit( EXIT_FAILURE );
  }
  
  fgets( line, sizeof( line ), f );   // get the first line (header)

  // get atmospheric load values
  int num = 0;
  while ( fgets( line, sizeof( line ), f ) ) {
    sscanf( line, "%[^,],%lf,%lf", date[num], &P[num], &E[num] );
    //printf( "%s %lf %lf\n", date[num], P[num], E[num] );
    num++;
  }

  fclose( f );
  return num;

}

/**
    @brief  Gets soil attribute values and initial conditions from input file.
    @param fname - input filename containing comma separated values (CSV)
                   consisting of a title line and a value line.
    @return string with input values.

    The input file consists of a title line followed by a value line.
    The value line has comma separated values consisting of: \n\n
    

    soil    - given soil name (string)

    comment - make a comment (string)

    uztwm   - upper layer (zone) tension water maximum capacity, mm. 
              The maximum volume of water (normalized to mm) held 
	      by the upper zone between field capacity and the wilting 
	      point which can be lost by direct evaporation and 
	      transpiration from soil. This storage is filled before 
	      any water in the upper zone is transferred to other storages.
	      Use field capacity and wilt point water content ratios 
	      to determine normalized volume (mm) for some depth. 
	      ie. (Ofc - Owp)*depth = uztwm (max et storage capacity) .

    uzfwm   - upper layer free water maximum capacity, mm. This storage is the 
              source of water for interflow and the driving force for 
              transferring water to deeper depths.

    uzk     - interflow depletion rate from the upper layer free water storage, 
              uzfwm per day. That is, lateral drainage rate of upper zone 
              free water expressed as a fraction of contents per day.
 
    pctim   - permanent impervious area fraction of the catchment which 
              produces impervious runoff during low flow conditions.

    adimp   - maximum fraction of an additional impervious area due to 
              saturation. That is, the additional fraction of the catchment 
              which exhibits impervious characteristics when the catchment's
              tension water requirements are met.

    perc    - maximum percolation rate coefficient (from upper zone free 
              water into the lower zone).

    rexp    - shape parameter of the percolation curve. An exponent 
              determining the rate of change of the percolation 
              rate with changing lower zone water contents.

    lztwm   - lower layer tension water maximum capacity, mm. Water from 
              this store can only be removed through transpiration.

    lzfsm   - lower layer supplemental free water maximum capacity, mm. 
              The maximum volume from which supplemental baseflow can be drawn.

    lzfpm   - lower layer primary free water maximum capacity, mm. The maximum
              capacity from which primary base flow can be drawn.

    lzsk    - depletion rate of the lower layer supplemental 
              free water storage, lzfsm per day. That is, lateral drainage rate
              of lower zone supplemental free water expressed as
              a fraction of contents per day.

    lzpk    - depletion rate of the lower layer primary free water storage, 
              lzfpm, per day. That is, lateral drainage rate of lower zone 
              primary free water expressed as a fraction of contents per day.

    pfree   - percolation fraction that goes directly to the lower layer free 
              water storages. Direct percolation fraction from upper to 
              lower zone free water (the percentage of percolated 
              water which is available to the lower zone free water 
              before all lower zone tension water deficiencies are satisfied).

    uztwc_0 - initial upper zone tension water content as proportion of ‘uztwm’

    uzfwc_0 - initial upper zone free water content as proportion of ‘uzfwm'

    lztwc_0 - initial lower zone tension water content as proportion of ‘lztwm’

    lzfsc_0 - initial lower zone free water secondary as proportion of ‘lzfsm’

    lzfpc_0 - initial lower zone free water primary as proportion of ‘lzfpm’

    adimc_0 - initial additional impervious flow store, as proportion of ‘uztwm+lztwm’


   NOTE: you can use the output values for uztwc, uzfwc, lztwc, lzfsc, lzfpc
and adimc as input values for another run (make sure dates match). Go to main() for output information.\n\n


Value ranges and default values: \n\n

            | Blasone et. al.(2008)  |   Surrgo (2004) |  default   | units
    uztwm   |        1-150           |    10-300       |   50.1     | mm
    uzfwm   |        1-150           |     5-150       |   40.2     | mm
    uzk     |      0.1-0.5           |  0.10-0.75      |   0.1      | frac/day
    pctim   | 0.000001-0.1           |     N/A         |   0.000001 | none
    adimp   |        0-0.4           |     N/A         |   0.0      | none
    zperc   |        1-250           |     5-350       |   1.0      | none
    rexp    |        0-5             |     1-5         |   0.0      | none
    lztwm   |        1-500           |    10-500       | 250.0      | mm
    lzfsm   |        1-1000          |     5-400       | 500.0      | mm
    lzfpm   |        1-1000          |    10-1000      | 500.0      | mm
    lzsk    |     0.01-0.25          |  0.01-0.35      |   0.01     | frac/day
    lzpk    |   0.0001-0.25          | 0.001-0.05      |   0.1      | frac/day
    pfree   |        0-0.6           |   0.0-0.8       |   0.1      | none

Soil input file example: \n\n
soil,comment,uztwm,uzfwm,uzk,pctim,adimp,zperc,rexp,lztwm,lzfsm,lzfpm,lzsk,lzpk,pfree,uztwc_0,uzfwc_0,lztwc_0,lzfsc_0,lzfpc_0,adimc_0\n
soil_default,demo purposes,50.1,40.2,0.1,0.000001,0.0,1.0,0.0,250.0,500.0,500.0,0.01,0.1,0.1,0.5,0.5,0.5,0.5,0.5,0.5
    
 */
char *read_soils( char *fname )
{
  char *line = calloc( 2048, 1 );

  // get soil attribute values and initial conditions from input file
  FILE *f = fopen( fname, "r" );
  if ( f == (FILE *)NULL )  {
    fprintf( stderr, "read_soils: error while opening the file %s.\n", fname );
    exit( EXIT_FAILURE );
  }

  // read headers; not robust !!!
  int found = 0; 
  while ( fgets( line, 2048, f) ) {
    if ( strcmp("soil",getfield(line,1)) == 0  && 
         strcmp("comment",getfield(line,2)) == 0 ) {
      found = 1;
      break;
    }
  }
 
  if ( !found ) {
   fprintf( stderr,"read_soils: cannot find start of variables soil parms.\n" );
   exit( EXIT_FAILURE );
  }

  // get soil attributes, just one line
  fgets( line, 2048, f );

  fclose( f );
  return( line );

}

/**
   @brief Replace climate model rain events with recorded local rain events.

   Allows user to use local rain event data instead of
   climate model rain values by supplying a data file.

   The file consists of a title line and value lines with 
   comma separated values (CSV). The values are a date followed
   by the value in millimeters. For example, \n \n

   Date,Rain (mm) \n
   2018-01-14,11.67 \n
   2018-01-15,17.27 \n
   2018-01-17,0.73 \n

   @param fname - path to precipitation CSV file (char *)
   @param num - number of entries in precipitation array
 */
void replace_precip( char *fname, int num ) 
{
  char line[2048];

  // open recorded rain event file
  FILE *f = fopen( fname, "r" );
  if ( f == (FILE *)NULL )  {
    fprintf( stderr, "replace_precip: error while opening the file %s.\n", 
	     fname );
    exit( EXIT_FAILURE );
  }

  fgets( line, sizeof( line ), f );   // get the first line (header)

  // zero out the precip array, P
  for ( int i=0; i<num; i++ ) P[i] = 0.0;

  // read dates and fill array elements
  char fecha[20];
  double rain;
  while ( fgets( line, sizeof( line ), f ) ) {
    if ( sscanf( line, "%[^,],%lf", fecha, &rain ) == 2 ) {

      // find corresponding date and replace with recorded value
      int found = 0;
      for ( int i=0; i<num; i++ ) {
        if ( strcmp( fecha, date[i] ) == 0 ) {
          P[i] = rain;
          found = 1;
          break;
        }
      }

      if ( !found ) {
        fprintf( stderr, "replace_precip: could not find date %s\n", fecha );
        //exit( EXIT_FAILURE );
      }
    }
  }

  fclose( f );

}

/**
   @brief Add irrigation water values to the precip array.
    This function reads the user supplied CSV irrigation file to supplement precip array values.

    The input file consists of a title line and value lines with 
    comma separated values (CSV). The values are a date followed
    by the value in millimeters. For example, \n \n

    Date,Irrigation (mm) \n
    2018-01-10,10.0     \n
    2018-01-20,10.0     \n
    2018-01-30,10.0     \n
    . \n
    . \n
    . \n
    
    @param fname - Irrigation input file name.
    @param num - number of entries to look through in the precipitation array.
*/
void add_irrigations( char *fname, int num ) 
{
  char line[2048];

  // open irrigation event file
  FILE *f = fopen( fname, "r" );
  if ( f == (FILE *)NULL )  {
    fprintf( stderr, "add_irrigations: error while opening the file %s.\n", 
	     fname );
    exit( EXIT_FAILURE );
  }

  fgets( line, sizeof( line ), f );   // get the first line (header)

  // read dates and compare
  char fecha[20];
  double irr;
  while ( fgets( line, sizeof( line ), f ) ) {
    if ( sscanf( line, "%[^,],%lf", fecha, &irr ) == 2 ) {

      // find corresponding date and add irrigation value to precip
      int found = 0;
      for ( int i=0; i<num; i++ ) {
        if ( strcmp( fecha, date[i] ) == 0 ) {
          P[i] += irr;
          found = 1;
          break;
        }
      }

      if ( !found ) {
        fprintf( stderr,"add_irrigations: could not find date %s\n", fecha );
        // not a reason to exit, since out of rundate range might of been used
      }
    }
  }

  fclose( f );
}

// FIXME: get value description into table like read_soils

/**
     @brief   Interface atmospheric loads and irrigation data with 
              the Sacramento SMA catchment model. 
     @return  File specified as output in the command line. 
     

     The output file has a title line followed by value lines. 
     The value line consists of:\n\n
 

     Date   - daily time stamp \n
     U      - the simulated effective rainfall (“total channel inflow”)\n
     uztwc  - upper zone tension water content \n
     uzfwc  - upper zone free water content \n
     lztwc  - lower zone tension water content \n
     lzfsc  - lower zone free secondary water content \n
     lzfpc  - lower zone free primary water content \n
     adimc  - tension water contents of the additional impervious area \n
     sett   - cumulative total evapotranspiration \n
     se1    - cumulative evapotranspiration from upper zone tension water \n
     se3    - cumulative evapotranspiration from lower zone tension water  \n
     se4    - cumulative evapotranspiration \n
     se5    - cumulative evapotranspiration from riparian zone \n
     roimp  - runoff from impervious area \n
     sdro   - six hour sum of runoff \n
     ssur   - surface runoff \n
     sif    - interflow \n
     bfp    - primary baseflow \n
     bfs    - secondary baseflow \n
     bfcc   - channel baseflow (bfp+bfs) \n

 */

int main( int argc, char *argv[] ) 
{
  set_files( argc, argv );               // parse options

  // get model atmospheric loads
  int num = read_loads( load_file );     // put into global P,E (precip, evap) 
                                         // arrays

  // get local data contributions, if provided
  if ( strcmp( rain_file, "" ) != 0 ) {
    replace_precip( rain_file, num );    // replace model rain for recorded 
  }                                      // rain events

  if ( strcmp( irr_file, "" ) != 0 ) {
    add_irrigations( irr_file, num );    // account for irrigation events
  }
 
  char *line = read_soils( soil_file );  // get soil attributes 
                                         // and initial conditions

  // read variable values from soil input line
  double xpar[13];
  for ( int i=0; i<13; i++ ) {
    xpar[i] = (double)atof( getfield(line,i+3) );
  }

  /* print out values (for debugging)
  printf( "uztwm = %lf\n", xpar[0] );  
  printf( "uzfwm = %lf\n", xpar[1] );  
  printf( "uzk   = %lf\n", xpar[2] );  
  printf( "pctim = %lf\n", xpar[3] );  
  printf( "adimp = %lf\n", xpar[4] );  
  printf( "zperc = %lf\n", xpar[5] );  
  printf( "rexp  = %lf\n", xpar[6] );  
  printf( "lztwm = %lf\n", xpar[7] );  
  printf( "lzfsm = %lf\n", xpar[8] );  
  printf( "lzfpm = %lf\n", xpar[9] );  
  printf( "lzsk  = %lf\n", xpar[10] ); 
  printf( "lzpk  = %lf\n", xpar[11] ); 
  printf( "pfree = %lf\n", xpar[12] ); 
*/

/* force defaults (for debugging)         
                      // defaults
  xpar[0] = 50.1;     // uztwm=50.1 
  xpar[1] = 40.2;     // uzfwm=40.2
  xpar[2] = 0.1;      // uzk=0.1    
  xpar[3] = 0.000001; // pctim=0.000001 
  xpar[4] = 0.0;      // adimp=0.0    
  xpar[5] = 1.0;      // zperc=1.0 
  xpar[6] = 0.0;      // rexp=0.0
  xpar[7] = 250.0;    // lztwm=250.0
  xpar[8] = 500.0;    // lzfsm=500.0
  xpar[9] = 500.0;    // lzfpm=500.0 
  xpar[10] = 0.01;    // lzsk=0.01
  xpar[11] = 0.1;     // lzpk=0.1
  xpar[12] = 0.1;     // pfree=0.1
*/

  // get initial conditions

  // initial upper zone tension water content as proportion of ‘uztwm’
  double uztwc_0 = (double)atof( getfield(line,16) );

  // initial upper zone free water content as proportion of ‘uzfwm'
  double uzfwc_0 = (double)atof( getfield(line,17) );

  // initial lower zone tension water content as proportion of ‘lztwm’
  double lztwc_0 = (double)atof( getfield(line,18) );

  // initial lower zone free water secondary as proportion of ‘lzfsm’
  double lzfsc_0 = (double)atof( getfield(line,19) );

  // initial lower zone free water primary as proportion of ‘lzfpm’
  double lzfpc_0 = (double)atof( getfield(line,20) );

  // initial additional impervious flow store, as proportion of ‘uztwm+lztwm’
  double adimc_0 = (double)atof( getfield(line,21) );

  cfree( line );

  /* for debugging
  printf( "uztwc_0 = %lf\n", uztwc_0 );
  printf( "uzfwc_0 = %lf\n", uzfwc_0 );
  printf( "lztwc_0 = %lf\n", lztwc_0 );
  printf( "lzfsc_0 = %lf\n", lzfsc_0 );
  printf( "lzfpc_0 = %lf\n", lzfpc_0 );
  printf( "adimc_0 = %lf\n", adimc_0 );
  */

  // hard wire these variables
  int    min_ninc = 20;     // minimum number of inner iterations. 
                            // used to improve numerical stability.
  double etmult   = 1.0;    // fudger et amplifier; set to unity
  double dt       = 1.0;    // increment in days
  
  // return values
  double U[num];            // the simulated effective rainfall 
                            // (“total channel inflow”), a time series 
                            // of the same length as the input series.

  double uztwc[num];        // upper zone tension water content
  double uzfwc[num];        // upper zone free water content
  double lztwc[num];        // lower zone tension water content
  double lzfsc[num];        // lower zone free secondary water content
  double lzfpc[num];        // lower zone free primary water content 
  double adimc[num];        // tension water contents of the additional 
                            // impervious area.
  double sett[num];         // cumulative total evapotranspiration
  double se1[num];          // cumulative evapotranspiration from upper 
                            // zone tension water.
  double se3[num];          // cumulative evapotranspiration from lower 
                            // zone tension water  
  double se4[num];          // cumulative evapotranspiration
  double se5[num];          // cumulative evapotranspiration from riparian zone
  double roimp[num];        // runoff from impervious area
  double sdro[num];         // six hour sum of runoff
  double ssur[num];         // surface runoff
  double sif[num];          // interflow
  double bfp[num];          // primary baseflow
  double bfs[num];          // secondary baseflow
  double bfcc[num];         // channel baseflow (bfp+bfs)

  // variables are sent as pointers due to fortran legacy.
  // run the Sacramento Model,
  sma_sac_state( P, E, &num,
                 xpar, &etmult,
                 &dt, U,
                 uztwc, uzfwc,
		 lztwc, lzfsc,
                 lzfpc, adimc,
                 sett, se1, se3,
                 se4, se5,
                 roimp, sdro,
                 ssur, sif,
                 bfp, bfs,
                 bfcc,
                 &uztwc_0, &uzfwc_0,
                 &lztwc_0, &lzfsc_0, &lzfpc_0,
                 &adimc_0, &min_ninc );

  // put results in csv format
  // check if stdout needs to be used
  FILE *f;
  if ( strcmp(out_file,"") == 0 ) {
    f = stdout;
  }
  else {
    f = fopen( out_file, "w" );
    if ( f == (FILE *)NULL )  {
      fprintf( stderr, "sma: error while opening the file %s.\n", out_file );
      exit( EXIT_FAILURE );
    }
  }
  
  // header
  fprintf( f, "Date,U,uztwc,uzfwc,lztwc,lzfsc,lzfpc,adimc,sett,se1,se3,se4,se5,roimp,sdro,ssur,sif,bfp,bfs,bfcc\n" );

  for ( int i=0; i<num; i++ ) {

    fprintf( f, "%s,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf\n", 
             date[i], U[i], uztwc[i], uzfwc[i], lztwc[i], lzfsc[i], 
             lzfpc[i], adimc[i], sett[i], se1[i], se3[i], se4[i], 
             se5[i], roimp[i], sdro[i], ssur[i], sif[i], bfp[i], 
             bfs[i], bfcc[i] );
  }

  fclose( f );
  return( EXIT_SUCCESS );

}

// end sma.c
