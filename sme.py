#! /usr/bin/env /usr/bin/python3

#  sme.py
# 
#  Copyright (c) 2018 IndieCompLabs, LLC.
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html
#

sme_copyright = 'sme.py Copyright (c) 2018 IndieCompLabs, LLC. ' + \
                'released under GNU GPL V3.0'
import os
import sys
import getopt
import datetime
import configparser
from glob import glob

import numpy as np
from PIL import Image
from netCDF4 import Dataset

from agrineer import logger
from agrineer import wrfbase
from crop_reader import crop_reader

## @file      sme.py
## @brief     Class and main program to estimate soil moisture from 
##            WRF generated weather data and crop indexed standard ET.
##            Depends on:\n
##            agrineer - https://gitlab.com/agrineer/agrineer \n
##
## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0

# TODO: list output files.

## Class sme is derived from class wrfbase.
class sme( wrfbase ):

    ## the constructor - no parameters
    def __init__( self ):
        
        wrfbase.__init__( self )

        ## Instantiation of crop_reader class to read crop file
        self.crop = None   
        self.clear_crop()
        
        ## Grow Degree Day value at run start date
        self.start_gdd = None

        ## Offset from planting date to run start date
        self.off = None

        ## Soil type file
        self.soilfile = None

    ## Load available crops from file.
    ## @param cropfile - filepath to crop file holding crop attributes
    ## @return boolean success state (True/False)
    def get_crop_options( self, cropfile ):

        try:
            return self.crop.read( cropfile )
        except:
            return False
    
    ## Clear out the Kci (crop coeffients) and crop arrays.    
    def clear_crop( self ):

        if self.crop != None:
            self.crop.clear()  

    ## Load crop attributes from available crop options and set plant date.
    ## @param name - crop name
    ## @param planted - plant date
    ## @return boolean success status (True/False)
    def load_crop( self, name, planted ):
        
        # find index according to crop name
        for i in range( 0, len( self.crop.info ) ):
            if self.crop.info[i][0] == name:
                break

        # get seasonal Kci's
        ok = self.crop.load_Kci( self.crop.info[i] )
        if not ok:
            # in case of bad index
            self.logger.append('\n cannot load crop attributes, bad index?\n')
            return False

        ok = self.crop.load_temp_params( self.crop.info[i] )
        if not ok:
            # in case of bad temps
            self.logger.append('\n cannot load crop GDD temp params\n')
            return False

        self.crop.name = name
        self.crop.planted = planted
        self.logger.append( '\n\tCrop:\t\t\t\t' + self.crop.name + '\n')
        self.logger.append( '\tDate Planted:\t\t' + self.crop.planted + '\n')

        return True

    ## Calculate this date's Grow Degree Day value.
    ## @param dfile - NetCDF4 file holding max and min temperatures
    ## @return day's GDD value
    def calc_GDD( self, dfile ):
        ds = Dataset( dfile, 'r' ) # open netcdf file
        
        tmin = ds.variables['TMIN'][self.sj,self.si]
        tmax = ds.variables['TMAX'][self.sj,self.si]

        # assign ceiling values
        # if max temp > upper temp; set to upper temp
        if tmax > self.crop.uppertemp:
            tmax = self.crop.uppertemp

        gdd = (tmax+tmin)/2.0 - self.crop.basetemp

        # make negative values zero
        if gdd < 0.0:
            gdd = 0.0

        return gdd

    ## Accumulate GDD values up to end date.
    ## Calculations do not include end date.
    ## Typically used to calculate GDD from plant date up to start date
    ## @param begin - begin date
    ## @param end - end date
    ## @return gdd value and number of days up to end date
    def accum_GDD( self, begin, end ):

        # get begin index
        num = len( self.data_names )
        for i in range(0,num):
            if self.data_dates[i] == begin:
                break

        gdd = 0.0
        ndays = 0

        # TODO: put calc_GDD here for faster implementation
        for j in range(i,num):
            if self.data_dates[j] == end:
                break

            gdd += self.calc_GDD( self.data_names[j] )
            ndays += 1
            
        return gdd,ndays

    ## Report values in the auxiliary rain and irr data files to the log
    ## @param file - data filepath
    def report_aux( self, file ):

        f = open( file )
        for line in f:
            date,value = line.split(',')
            self.logger.append( '\t' + date + '\t\t' + value )
        f.close() 
        
    ## Run the Sacramento Soil Moisture Accounting (SMA) model
    ## by invoking the sma program.
    def run_sma( self ):

        # read the SMA input file and report to logger
        soils = open( self.soilfile, 'r' )

        found = False
        while True:
            line = soils.readline()
            if not line:
                break
            items = line.split(',')
            if items[0] == 'soil' and items[1] == 'comment':
                found = True
                break
            
        if not found:
            self.logger.append( 
                '\n ERROR: could not read SMA input file header correctly.\n' )
            soils.close()
            return
        
        line = soils.readline()  # read soil attributes
        soils.close()
        
        items = line.split(',')

        if len(items) != 21:
            self.logger.append( 
                '\n ERROR: could not read SMA input line correctly, should have 21 items.\n' )
            return
        
        # report values
        self.logger.append( '\n Soil attributes used in Sacramento Soil Moisture Accounting model:\n')
        self.logger.append( '\tname      = ' + items[0] + '\n')
        self.logger.append( '\tcomment   = ' + items[1] + '\n')
        
        self.logger.append( '\tuztwm     = ' + items[2] + '\n')
        self.logger.append( '\tuzfwm     = ' + items[3] + '\n')
        self.logger.append( '\tuzk       = ' + items[4] + '\n')

        self.logger.append( '\tpctim     = ' + items[5] + '\n')
        self.logger.append( '\tadimp     = ' + items[6] + '\n')
        self.logger.append( '\tzperc     = ' + items[7] + '\n')
        self.logger.append( '\trexp      = ' + items[8] + '\n')

        self.logger.append( '\tlztwm     = ' + items[9] + '\n')
        self.logger.append( '\tlzfsm     = ' + items[10] + '\n')
        self.logger.append( '\tlzfpm     = ' + items[11] + '\n')
        self.logger.append( '\tlzsk      = ' + items[12] + '\n')
        self.logger.append( '\tlzpk      = ' + items[13] + '\n')

        self.logger.append( '\tpfree     = ' + items[14] + '\n')

        self.logger.append( '\tuztwc_0   = ' + items[15] + '\n')
        self.logger.append( '\tuzfwc_0   = ' + items[16] + '\n')
        self.logger.append( '\tlztwc_0   = ' + items[17] + '\n')
        self.logger.append( '\tlzfsc_0   = ' + items[18] + '\n')
        self.logger.append( '\tlzfpc_0   = ' + items[19] + '\n')
        self.logger.append( '\tadimc_0   = ' + items[20] + '\n\n')

        # construct the configuration file for the SMA
        smaconf = open( self.outdir + '/sma.conf', 'w' )
        smaconf.write( '[FILES]\n' )
        smaconf.write( 'load = ' + self.outdir + '/loads.csv\n' )
        smaconf.write( 'soil = ' + self.soilfile + '\n' )
        smaconf.write( 'out  = ' + self.outdir + '/sma.csv\n' )

        # check if rain data is included
        if os.path.isfile( self.rainfile ):
            smaconf.write( 'rain = ' + self.rainfile + '\n' )
            self.logger.append( '\n\n Local recorded rain events, overides model rain events:\n' )
            self.report_aux( self.rainfile )
        else:
            smaconf.write( 'rain = \n' )
            
        # check if irrigation data is included
        if os.path.isfile( self.irrfile ):
            smaconf.write( 'irr  = ' + self.irrfile + '\n' )
            self.logger.append( '\n\n Recorded irrigation events:\n' )
            self.report_aux( self.irrfile )
        else:
            smaconf.write( 'irr  = \n' )

        # done writing configuration file
        smaconf.close()

        # set up soil moisture command line
        command = self.soilprog + ' -c ' + self.outdir + '/sma.conf'

        # attach error log path
        sma_err_file = self.outdir + '/sma_error.log'
        command += ' 2> ' + sma_err_file
        status = os.system( command )
        if status != 0:
            self.logger.append( 
                '\n\n ERROR: soil moisture model did not run correctly.\n' )
        else:
            # report end rundate's values
            sma_out = open( self.outdir + '/sma.csv' ) # open SMA output file
            for line in sma_out:                       # read until last line
                pass
            sma_out.close()
            items = line.split(',')
            
            # report            
            self.logger.append( 'Soil top level total water content = ' +
                                items[2] + ' mm.\n' )
            self.logger.append( 'Soil bottom level total water content = ' +
                                items[4] + ' mm.\n' )
                
        # check for messages from error log  
        size = os.path.getsize( sma_err_file )
        if size > 0:
            
            # write out messages
            efile = open( sma_err_file )
            self.logger.append( "\n\n SMA messages:\n\n" )
            for line in efile:
                self.logger.append( '\t' + line )
            efile.close
         
    ## Report values of GDD, ETo, ETc, and Rain events for rundates 
    ## at selected pixel. Also writes to file atmospheric loads for
    ## soil moisture model.
    def report_pixel( self ):

        # allocate and zero out accumulation data arrays
        # FIXME: hate fixed values
        eto_total = np.zeros( (171,171), dtype=np.float32 )
        precip_total = np.zeros( (171,171), dtype=np.float32 )

        gdd = self.start_gdd      # current GDD for our pixel
        begin = self.off          # num days since crop planted 
                                  # and before start

        Kci = 0.0                 # initialize crop coefficent 
                                  # to value never used
        etc_total = 0.0           # estimated ET with crop coefficient applied

        exceeded = False          # flag to print message about exceeding
                                  # seasonal length given by crop info

        # time-series output; used in soil moisture models
        # as atmospheric loads
        series = open( self.outdir + '/loads.csv', 'w' )
        series.write( 'Date,P,E\n' )    
        
        end = len( self.data_names )        
        for i in range( begin, end ):

            # get this day's crop coefficient, 
            # if run date exceeds seasonal length use last Kci value
            try:
                K = self.crop.Kci[i]
                if K != Kci:                            # check for change
                    Kci = K
                    self.logger.append( ' Using Kci=' + '%.2f'%Kci + 
                                            ' on day ' + str(i+1) + 
                                            ' of season. \n')
            except:
                if not exceeded:
                    self.logger.append( 
                        '\n Season length has been exceeded, using last Kci value.\n')
                    exceeded = True                     # write message out once
                    
            ds = Dataset( self.data_names[i], 'r' ) # open the netcdf file
            if self.verbose:
                self.logger.append( ' working on file: ' + self.data_names[i] 
                                    + '\n' )

            eto = ds.variables['STDEVP'][:]        # gets data array the fastest
                                                   # significantly 20x for below
                                                   # can also get array as:
                                                   # ds['STDEVP'], and
                                                   # ds.variables['STDEVP'],
                                                   # impressive implementation
    
            eto_total += eto                       # accumulate array total

            etc = eto[ self.sj,self.si ]*Kci       # use crop coefficient 
                                                   # against eto pixel value
            etc_total += etc

            precip = ds.variables['PRECIP'][:]     # days precip array
            precip_total += precip

            rain = precip[ self.sj,self.si ]       # pixel precip value
            if rain > 0.0:
                self.logger.append( ' Rain event:\t' + '%.2f'%rain  + 'mm' +
                ' on ' + self.data_dates[i] + '\n' )
            
            # output atmospheric loads (ETc and Rain) values for soil model
            day = self.data_dates[i]
            fecha = day[:4] + '-' + day[4:6] + '-' + day[6:8] # format date

            # precip and eto values are calculated to 4 decimal points
            series.write( fecha + ',' + '%.4f'%rain + ',' + '%.4f'%etc + '\n' )
                          
            # accumulate GDD value
            tmin = ds.variables['TMIN'][self.sj,self.si]
            tmax = ds.variables['TMAX'][self.sj,self.si]

            # assign temperature ceiling
            if tmax > self.crop.uppertemp:
                tmax = self.crop.uppertemp

            temp_gdd = (tmax+tmin)/2.0 - self.crop.basetemp
            if temp_gdd > 0:
                gdd += temp_gdd

        # end for loop

        series.close() # close the data series file

        # report to log
        self.logger.append( '\n Cumulative values, including end date:\n' ) 
        self.logger.append( '\tSince plant date:\n' ) 
        self.logger.append( '\t\tGDD :\t' + '%.2f'%gdd + 'C\n' )

        self.logger.append( '\tSince start date:\n' ) 
        eto_pixel_total = eto_total[ self.sj,self.si ]

        self.logger.append( '\t\tTotal ETo:\t' + '%.2f'%eto_pixel_total  
                           + 'mm\n' )

        self.logger.append( '\t\tTotal ETc:\t' + '%.2f'%etc_total + 'mm\n' )
        
        precip_pixel_total = precip_total[ self.sj,self.si ]
        self.logger.append( '\t\tTotal Precip:\t' + '%.2f'%precip_pixel_total + 
                           'mm\n')

        self.logger.append( '\tSurface Deficits (negative value means surplus):\n' )

        deficit = eto_total[self.sj,self.si] - precip_pixel_total 
        self.logger.append( '\t\tEto-Precip =\t' + '%.2f'%deficit + 'mm\n' )

        deficit = etc_total - precip_pixel_total 
        self.logger.append( '\t\tEtc-Precip =\t' + '%.2f'%deficit + 'mm\n' )

    ## Accumulate values of eto, precip for a sector domain. 
    ## @return standard eto and precip_total values arrays.
    def accumulate( self ):

        # allocate and zero out accumulation data arrays
        # FIXME: hate fixed values, get size from ds
        eto_total = np.zeros( (171,171), dtype=np.float32 )
        precip_total = np.zeros( (171,171), dtype=np.float32 )

        # data_names is set globally (in run() method)
        num = len( self.data_names ) # number of run data files 

        # accumulate values for this domain
        for i in range( 0, num ):

            ds = Dataset( self.data_names[i], 'r' )  # open the netcdf file
            eto = ds.variables['STDEVP'][:]          
            precip = ds.variables['PRECIP'][:]
        
            eto_total += eto
            precip_total += precip

        return eto_total, precip_total

    ## Load configuration file values
    def load_values( self, config_file ):

        # start configuration 
        config = configparser.RawConfigParser()        
        config.read( config_file )  

        # read and load I/O directories
        indir = config.get( 'I/O Directories', 'input' )
        outdir = config.get( 'I/O Directories', 'output' )

        ok = self.load_dirs( indir, outdir )
        if not ok:
            self.logger.append( '\tERROR: cannot load I/O dirs: ' + indir + ' ' 
                               + outdir )
            sys.exit( 10 )        # our exit 10 indicates bad dirs

        # load crop options and select
        cropfile = config.get( 'Crop', 'file' )
        ok = self.get_crop_options( cropfile )
        if not ok:
            self.logger.append( '\tERROR: cannot load crop options file: ' + 
                                cropfile )
            sys.exit( 11 )        # our exit 11 indicates bad crop filename


        name = config.get( 'Crop','name' )
        planted = config.get( 'Crop','plantdate' )
        ok = self.load_crop( name, planted )
        if not ok:
            self.logger.append( '\tERROR: cannot load crop or plant date: ' 
                               + name + ', ' + planted ) 
            sys.exit( 12 )        # our exit 12 indicates bad crop info

        # read and load location
        lat = config.get( 'Location','latitude' )
        lon = config.get( 'Location','longitude' ) 
        ok = self.load_geo( float(lat), float(lon), True )
        if not ok:
            self.logger.append( '\tERROR: cannot load coordinates: ' + lat + 
                                ',' + lon )
            sys.exit( 13 )        # our exit 13 indicates bad location

        # exit 14 is given for bad rundates, but not here

        # get the soil model
        self.soilmodel = config.get( 'SoilModel','model' )

        # TODO: check for known model here 

        # path to soil model program
        self.soilprog = config.get( self.soilmodel,'program' )
        if not os.path.isfile( self.soilprog ):
            self.logger.append( '\tERROR: cannot load soil moisture model: ' 
                                + self.soilmodel+' program: ' + self.soilprog )
            sys.exit( 15 )        # our exit 15 indicates bad soilmodel

        # soil file type filename
        self.soilfile = config.get( self.soilmodel,'soil' )
        if not os.path.isfile( self.soilfile ):
            self.logger.append( '\tERROR: cannot load soil file: ' + 
                                soilfile )
            sys.exit( 15 )        # our exit 15 indicates bad soilmodel 

        # get auxiliary rain and irrigation data for this site

        ## Auxiliary local rain event file, overrides WRF rain events.
        self.rainfile = ''

        ## Auxiliary irrigation file, adds irrigation events to precipitation.
        self.irrfile = ''

        try:
            # files gets checked later
            self.rainfile = config.get( self.soilmodel,'rain' ) 
            self.irrfile = config.get( self.soilmodel,'irr' ) 
        except:
            pass

        ## Start date for run, from wrfbase.
        self.start = config.get( 'Rundates','start' )

        ## End date for run, from wrfbase.
        self.end = config.get( 'Rundates','end' )
            
    # end load_values

    ## Run soil moisture model on specified pixel and crop.
    def calc_pixel_ETc( self ):

        # prepare data according to WRF domain  
        ok = self.load_filenames( self.crop.planted, self.end, 'd03', 
                                  self.sector )
        if not ok:
            self.logger.append( '\tERROR: cannot load rundates: ' + 
                                self.crop.planted + ', ' + self.end )
            sys.exit( 14 )        # our exit 14 indicates bad rundates

        # accumulate GDD up to start date and return 
        # number of days from planted up to start; 
        # gets offset to data, date lists
        self.start_gdd, self.off = self.accum_GDD( self.crop.planted, 
                                                   self.start )
            
        # report dates used for run
        ndays = len( self.data_names )
        self.logger.append( '\n Dates:\n' )
        self.logger.append( '\t\tplanted:\t\t' + self.data_dates[0] + '\n' )
        self.logger.append( '\t\tstart:\t\t\t' + self.data_dates[self.off] + 
                            '\n' )
        self.logger.append( '\t\tend:\t\t\t' + self.data_dates[ndays-1] + 
                            '\n\n' )
        self.logger.append( '\t\tdays since planted:\t' + str(ndays) + '\n' )
        self.logger.append( '\t\tnum of run days :\t' + str(ndays-self.off) + 
                            '\n' )

        self.report_pixel() # get atmospheric loads

        # run soil moisture model based on loads
        if self.soilmodel == 'SMA':
            self.run_sma()      
        else:
            print( 'calc_pixel_ETc: bad soil model: ' 
                   + self.soilmodel + ' exiting.', file=sys.stderr, flush=True )
            sys.exit( 1 )

    # a more generic version for 10km, 30km domains
    def run( self, domain ):

        if domain == 'd03':
            res = '3km'
        elif domain == 'd02':
            res = '10km'
        elif domain == 'd01':
            res = '30km'
        else:
            print( 'run: bad domain given: ' + domain, 
                   file=sys.stderr, flush=True )
            sys.exit( 1 )        
            
        # prepare data according to WRF domain  
        ok = self.load_filenames( self.start, self.end, domain, self.sector )
        if not ok:
            self.logger.append( '\tERROR: cannot load rundates: ' + self.start 
                                + ', ' + self.end )
            sys.exit( 14 )        # our exit 14 indicates bad rundates

        eto_accum, precip_accum = self.accumulate()
        self.save_image( eto_accum, False, 
                         outdir + '/eto_' + res + '.png' )

        self.save_image( precip_accum, True, 
                         outdir + '/precip_' + res + '.png' )

        # construct data, lat, long data files
        geo_path = self.indir + '/static_' + res + '/' + self.sector + \
        '_geo_' + domain + '.nc'

        self.save_data( eto_accum, geo_path, 
                        outdir + '/eto_' + res + '.data' )

        self.save_data( precip_accum, geo_path, 
                        outdir + '/precip_' + res + '.data' )

# end class sme

# ------------------------------------------------------------------

## Print to screen usage and parameters
def usage():
    print('usage: sme.py', file=sys.stderr, flush=True)
    print('       -h, --help', file=sys.stderr, flush=True)
    print('       -c config_file, --config=config_file', 
          file=sys.stderr, flush=True)

## Parse command line arguments
## @param argv - command line string            
def set_params( argv ):
    incsv = None

    try:                                
        opts, args = getopt.getopt( argv, 'hc:', ['help','config='] )
    except:
        usage()                          
        sys.exit(16)   # our exit 16 indicates bad args
                   
    for opt, arg in opts:                
        if opt in ( '-h', '--help' ):     # our exit 17 indicates help request  
            usage()                     
            sys.exit(17)   

        elif opt in ( '-c', '--config' ):    
            if os.path.isfile( arg ):
                incsv = arg
            else:
                print( 'Configuration file: ' + arg + ' does not exist.',
                       file=sys.stderr, flush=True )
                sys.exit(18)   

    if incsv == None:
        print('sme.py: must specify config file',
              file=sys.stderr, flush=True)
        sys.exit(18)  # our exit 18 indicates no config file

    return incsv

## Main entrance
if __name__ == '__main__':  

    outdir = None
    try:
        # command line gives config file
        config_file = set_params( sys.argv[1:] )

        # construct our log filename
        conf = configparser.RawConfigParser()        
        conf.read( config_file )

        outdir = conf.get( 'I/O Directories', 'output' )
        logfile = outdir + '/sme.log'
 
    except:
        print('sme.py: cannot load configuration file:',
                config_file, file=sys.stderr, flush=True)
        sys.exit(18)          # our exit 18 indicates bad config file

    sme = sme()               # instantiate SME class
    #sme.verbose = True

    try:
        sme.logger = logger( logfile )
    except:
        print('sme.py: cannot load logfile:', logfile,
              file=sys.stderr, flush=True)
        sys.exit(19)          # our exit 19 indicates bad logfile

    # sme.logger.append( '\t' + sme.version + '\n' )

    if sme.verbose:
        sme.logger.append( '\tconfiguration file:\t' + config_file + '\n' )

    sme.crop = crop_reader( sme.logger )   # instantiate crop reader
                                           # doesn't actually read yet
    sme.load_values( config_file )            

    # get images and data files for each domain and all pixels
    sme.run( 'd01' )                       # 30km domain
    sme.run( 'd02' )                       # 10km domain
    sme.run( 'd03' )                       # 3km domain

    # calculate and report total GDD and ETc values for selected pixel 
    # in 3km domain
    sme.calc_pixel_ETc()

# end sme.py
