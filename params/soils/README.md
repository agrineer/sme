## Description

The smv_default.csv input file is used for test purposes. You will need to construct your own input file according to the instructions found at the sme [wiki](https://www.agrineer.org/wiki/index.php?title=Soil_Moisture_Estimator).