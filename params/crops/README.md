## Description

These crop coefficient values are from this FAO [website](
http://www.fao.org/docrep/X0490E/x0490e0b.htm).

The comments and notes found there are very useful and should be read by the user.

The user should be diligent in providing specific crop coefficient values for the cultivar being modeled. 

## Note
In addition to the ETc variables provided by the FAO, we have appended Grow Degree Day (GDD) temperature parameters at the end of the input line to report Grow Degree Day calculations. These values are most likely arbitrary and have NOT been verified and are used for exercising the model only. 


It is our goal to create a library of local cultivar irrigation coeffients 
and GDD thresholds. Stay tuned.


